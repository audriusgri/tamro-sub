import { IUser } from '../Interfaces/IUser';

export class User implements IUser {
  public id: number;
  public email: string;
  public fullName: string;
  public dob: Date;
  public userName: string;
  public password: string;
  public additionalText?: string;
}
