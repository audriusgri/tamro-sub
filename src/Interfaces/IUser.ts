export interface IUser {
    id: number;
    email: string;
    fullName: string;
    dob: Date;
    userName: string;
    password: string;
    additionalText?: string;
}
