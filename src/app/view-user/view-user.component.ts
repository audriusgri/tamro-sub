import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { User } from '../../Models/User';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {
    users: User[] = [];
    title = 'Users list';
    selectedUser: User;
    searchString;
  constructor(private usersService: UsersService) {
      this.usersService.getUsers().subscribe(res => {
        this.users = res as User[];
      });
  }

  ngOnInit() {
  }

    onClick(user: User): void {
      this.selectedUser = user;
  }
}
