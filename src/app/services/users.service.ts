import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators';
import { User } from '../../Models/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    baseUrl = '';
    routerPath = 'api/users';

  constructor(private _http: HttpClient) {
    this.baseUrl = environment.baseUrl;
  }

    getUsers() {
      return this._http.get(`${this.baseUrl}${this.routerPath}`)
        .pipe(map(
          response => {
            return response;
          }));
    }

    getUserById(id: number): Observable<User> {
        return this._http.get<User>(`${this.baseUrl}${this.routerPath}/${id}`, {
          headers: new HttpHeaders({
            'Content-Type': 'application/json'
          })
        })
        .pipe(map(
          response => {
            return response;
          }));
    }

    addUser(user: User): Observable<User> {
        return this._http.post<User>(`${this.baseUrl}${this.routerPath}/`, user, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }).pipe(map(
            response => {
                return response;
            }));
    }

    updateUser(user: User): Observable<User> {
        return this._http.put<User>(`${this.baseUrl}${this.routerPath}/${user.id}`, user, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        })
            .pipe(map(
                response => {
                    return response;
                }));
    }
}
