import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../services/users.service';
import { User } from "../../Models/User";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
    @Input() selectedUserInput: User;
    private userForm: FormGroup;
    private id: FormControl;
    private fullName: FormControl;
    private email: FormControl;
    private dob: FormControl;
    private userName: FormControl;
    private password: FormControl;
    private additionalText: FormControl;

    constructor(private formBuilder: FormBuilder, private router: Router, private usersService: UsersService) {
      this.id = new FormControl('', [Validators.required]);
      this.email = new FormControl('', [Validators.required]);
      this.fullName = new FormControl('', [Validators.required]);
      this.dob = new FormControl('', [Validators.required]);
      this.userName = new FormControl('', [Validators.required]);
      this.password = new FormControl('', [Validators.required]);
      this.additionalText = new FormControl('');

      this.userForm = this.formBuilder.group({
        id: this.id,
        email: this.email,
        fullName: this.fullName,
        dob: this.dob,
        userName: this.userName,
        password: this.password,
        additionalText: this.additionalText,
    });
  }
  title = 'Update';
    ngOnInit() {
    }

  ngOnChanges() {
    if (this.selectedUserInput) {
      this.userForm.setValue(this.selectedUserInput);
    }
  }

  onClickSave(): void {

    this.usersService.updateUser(this.userForm.value).subscribe(
      () => {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate(['/users']));
      }, error => console.error(error));
  }

  cancel(): void {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate(['/users']));
  }

}
