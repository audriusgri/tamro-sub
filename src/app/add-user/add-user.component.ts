import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { User } from "../../Models/User";
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
    title = 'Add';
    userForm: FormGroup;
    users;
    mappingForm: FormGroup;
    constructor(private _router: Router,
      private usersService: UsersService,
      private formBuilder: FormBuilder) {
        this.users = this.getUsers();
        this.userForm = this.formBuilder.group({
        email: ['', [Validators.required]],
        fullName: ['', [Validators.required]],
        dob: ['', [Validators.required]],
        userName: ['', [Validators.required]],
        password: ['', [Validators.required]],
        additionalText: ['', [Validators.required]]
      });
    }

  ngOnInit() {
  }
  getUsers() {
    this.usersService.getUsers().subscribe(
      (data: User[]) => this.users = data
    );
  }

    onSubmit(userData: User) {
      console.log(userData);
      if (userData.fullName.trim().length == 0 ||
        userData.email.trim().length == 0 ||
          userData.userName.trim().length == 0 ||
          userData.password.trim().length == 0) { //better use isnullorwhitespace and add other check for date etc, special chars
        alert('You must specify all the parameters. Please try again');
        return;
    }
      this.usersService.addUser(userData).subscribe(
      () => {
        this._router.navigate(['/users']);
      }, error => console.error(error));
  }

  cancel() {
      this._router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this._router.navigate(['/users']));
  }
}
